EXE_NAME     = stlviewer

SRC_DIR      = src/

SOURCES_C    = foo.c
SRC_C        = $(addprefix $(SRC_DIR),$(SOURCES_C))
OBJ_C        = $(patsubst %.c,%.o,$(SRC_C))
ASM_C        = $(OBJ_C:.o=.s)
DEP_C        = $(OBJ_C:.o=.d)

SOURCES_CPP  = viewer.cpp mesh.cpp model.cpp renderer.cpp stl.cpp
SRC_CPP      = $(addprefix $(SRC_DIR),$(SOURCES_CPP))
OBJ_CPP      = $(patsubst %.cpp,%.o,$(SRC_CPP))
ASM_CPP      = $(OBJ_CPP:.o=.s)
DEP_CPP      = $(OBJ_CPP:.o=.d)

GCC_VER      = 9
CC           = gcc-$(GCC_VER)
CXX          = g++-$(GCC_VER)

#M32          = -m32

L_CPPFLAGS  += -O3 -g0 -DNDEBUG
#L_CPPFLAGS  += -O0 -g3 -gdwarf-4
L_CPPFLAGS  += -Werror -Wall -Wextra -pedantic
L_CPPFLAGS  += -march=native -mtune=native
#L_CPPFLAGS  += -march=native -mtune=native -mcpu=sandybridge
L_CPPFLAGS  += $(M32)
L_CPPFLAGS  += -I./src
L_CPPFLAGS  += -I./include

L_CFLAGS    += -std=c11

L_CXXFLAGS  += -std=c++2a
L_CXXFLAGS  += -fconcepts

L_LDFLAGS   += $(M32)

# ifeq ($(M32), -m32)
# L_LDLIBS    += -lboost_system
# else
# L_LDLIBS    += -lboost_system
# endif
L_LDLIBS    += -pthread
L_LDLIBS    += -lGL -lGLU -lglut

override CPPFLAGS   += $(L_CPPFLAGS)
override CFLAGS     += $(L_CFLAGS)
override CXXFLAGS   += $(L_CXXFLAGS)

override LDFLAGS    += $(L_LDFLAGS)
override LOADLIBES  += $(L_LOADLIBES)
override LDLIBS     += $(L_LDLIBS)


%.o: %.c Makefile
	$(CC) $(CPPFLAGS) $(CFLAGS) -MMD -MP -c $< -o $@

%.o: %.cpp Makefile
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MMD -MP -c $< -o $@

all: $(OBJ_C) $(OBJ_CPP)
	$(CXX) $(LDFLAGS) $(LOADLIBES) $(LDLIBS) -o $(EXE_NAME) $^ $(LDLIBS)

clean:
	rm -f $(EXE_NAME)
	rm -f $(OBJ_C) $(ASM_C) $(DEP_C)
	rm -f $(OBJ_CPP) $(ASM_CPP) $(DEP_CPP)
	rm -f *.s

asm: asmc asmcpp

asmc: $(SRC_C)
	$(CC) $(CPPFLAGS) $(CFLAGS) -g0 -S $^

asmcpp: $(SRC_CPP)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -g0 -S $^

-include $(DEP_C)
-include $(DEP_CPP)
