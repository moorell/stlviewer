#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP


#include <array>
#include <cmath>


template <typename T = float, std::size_t N = 4>
using vector = std::array<T,N>;

template <typename T = float, std::size_t R = 4, std::size_t C = 4>
using matrix = std::array<vector<T,C>,R>;


template <typename T = float, std::size_t R = 4, std::size_t C = 4>
constexpr inline vector<T,C> row(const matrix<T,R,C>& m, unsigned r) noexcept
{
    return m[r];
}


template <typename T = float, std::size_t R = 4, std::size_t C = 4>
constexpr inline vector<T,R> column(const matrix<T,R,C>& m, unsigned c) noexcept
{
    vector<T,R> col;

    for (unsigned r = 0; r < R; ++r)
        col[r] = m[r][c];

    return col;
}


template <typename T = float, std::size_t N = 4>
constexpr inline T dot_product(const vector<T,N>& v1, const vector<T,N>& v2) noexcept
{
    T res{};

    for (unsigned i = 0; i < N; ++i)
        res += v1[i] * v2[i];

    return res;
}


using vector4 = vector<float,4>;
using matrix4 = matrix<float,4,4>;


constexpr inline vector4 row(const matrix4& m, unsigned r) noexcept
{
    return m[r];
}


constexpr inline vector4 column(const matrix4& m, unsigned c) noexcept
{
    return { m[0][c], m[1][c], m[2][c], m[3][c] };
}


constexpr inline vector4 operator~(const vector4& v) noexcept
{
    return { -v[0], -v[1], -v[2], v[3] }; // Last element unchanged on purpose.
}


constexpr inline bool operator==(const vector4& v1, const vector4& v2) noexcept
{
    return v1[0] == v2[0] && v1[1] == v2[1] && v1[2] == v2[2] && v1[3] == v2[3];
}


constexpr inline bool operator!=(const vector4& v1, const vector4& v2) noexcept
{
    return v1[0] != v2[0] || v1[1] != v2[1] || v1[2] != v2[2] || v1[3] != v2[3];
}


constexpr inline vector4 operator+(const vector4& v1, const vector4& v2) noexcept
{
    return { v1[0] + v2[0], v1[1] + v2[1], v1[2] + v2[2], std::max(v1[3], v2[3]) };
}


constexpr inline vector4 operator+(const vector4& v1, const float value) noexcept
{
    return { v1[0] + value, v1[1] + value, v1[2] + value, v1[3] };
}


constexpr inline vector4 operator-(const vector4& v1, const vector4& v2) noexcept
{
    return { v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2], std::max(v1[3], v2[3]) };
}


constexpr inline vector4 operator-(vector4 v1, const float value) noexcept
{
    return { v1[0] - value, v1[1] - value, v1[2] - value, v1[3] };
}


constexpr inline vector4 operator*(const vector4& v1, const vector4& v2) noexcept
{
    return { v1[0] * v2[0], v1[1] * v2[1], v1[2] * v2[2], std::max(v1[3], v2[3]) };
}


constexpr inline vector4 operator*(const vector4& v1, const float value) noexcept
{
    return { v1[0] * value, v1[1] * value, v1[2] * value, v1[3] };
}


constexpr inline vector4 operator/(const vector4& v1, const vector4& v2) noexcept
{
    return { v1[0] / v2[0], v1[1] / v2[1], v1[2] / v2[2], std::max(v1[3], v2[3]) };
}


constexpr inline vector4 operator/(const vector4& v1, const float value) noexcept
{
    return { v1[0] / value, v1[1] / value, v1[2] / value, v1[3] };
}


constexpr inline vector4 operator*(const matrix4& m, const vector4& v) noexcept
{
    return { m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3],
             m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3],
             m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3],
             m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3] };
}


constexpr inline matrix4 operator*(const matrix4& m1, const matrix4& m2) noexcept
{
    matrix4 mr{};

    for (unsigned i = 0; i < m1.size(); ++i)
    {
        vector4 r = row(m1, i);

        for (unsigned j = 0; j < m1.size(); ++j)
        {
            vector4 c = column(m2, j);
            mr[i][j] = dot_product(r, c);
        }
    }

    return mr;
}


inline matrix4 create_rotx_matrix(float thx)
{
    thx *= M_PI / 180.0f;

    matrix4 m = {
        1, 0, 0, 0,
        0, cosf(thx), -sinf(thx), 0,
        0, sinf(thx), cosf(thx), 0,
        0, 0, 0, 0
    };

    return m;
}


inline matrix4 create_roty_matrix(float thy)
{
    thy *= M_PI / 180.0f;

    matrix4 m = {
        cosf(thy), 0, sinf(thy), 0,
        0, 1, 0, 0,
        -sinf(thy), 0, cosf(thy), 0,
        0, 0, 0, 0
    };

    return m;
}


namespace std {


constexpr inline vector4 max(const vector4& v1, const vector4& v2) noexcept
{
    return { std::max(v1[0], v2[0]), std::max(v1[1], v2[1]), std::max(v1[2], v2[2]), std::max(v1[3], v2[3]) };
}


constexpr inline vector4 min(const vector4& v1, const vector4& v2) noexcept
{
    return { std::min(v1[0], v2[0]), std::min(v1[1], v2[1]), std::min(v1[2], v2[2]), std::min(v1[3], v2[3]) };
}


} // namespace std


// Cross product includes only the first three coordinates.
constexpr inline vector4 cross_product(const vector4& v1, const vector4& v2) noexcept
{
    return { v1[1] * v2[2] - v1[2] * v2[1],
             v1[2] * v2[0] - v1[0] * v2[2],
             v1[0] * v2[1] - v1[1] * v2[0],
             std::max(v1[3], v2[3]) };
}


constexpr inline float dot_product(const vector4& v1, const vector4& v2) noexcept
{
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2] + v1[3] * v2[3];
}


// Dot product includes only the first three coordinates.
constexpr inline float dot_product(const vector4& v) noexcept
{
    return v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
}


inline float length(const vector4& v) noexcept
{
    return std::sqrt(dot_product(v));
}


inline vector4 normalized(const vector4& v) noexcept
{
    float len = length(v);

    if (0.0f == len)
        len = 1.0f;

    return v / len;
}


inline vector4 normal(const vector4& vb, const vector4& ve) noexcept
{
    return normalized(cross_product(vb, ve));
}


inline float distance(vector4 const& v1, vector4 const& v2) noexcept
{
    vector4 v = v1 - v2;

    return sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}


inline float distancexy(vector4 const& v1, vector4 const& v2) noexcept
{
    vector4 v = v1 - v2;

    return sqrt(v[0]*v[0] + v[1]*v[1]);
}


inline float angle(vector4 const& v1, vector4 const& v2, bool deg = true) noexcept
{
    float ang = acos(dot_product(v1 * v2));
    if (deg)
        ang *= 180.0f / M_PI;

    return ang;
}


constexpr inline vector4 zero_position = { 0.0f, 0.0f, 0.0f, 1.0f };
constexpr inline vector4 zero_vector   = { 0.0f, 0.0f, 0.0f, 0.0f };


constexpr inline matrix4 identity_matrix = { 1.0f, 0.0f, 0.0f, 0.0f,
                                             0.0f, 1.0f, 0.0f, 0.0f,
                                             0.0f, 0.0f, 1.0f, 0.0f,
                                             0.0f, 0.0f, 0.0f, 1.0f };


constexpr inline matrix4 reverse_matrix = { 0.0f, 0.0f, 1.0f, 0.0f,
                                            0.0f, 1.0f, 0.0f, 0.0f,
                                            1.0f, 0.0f, 0.0f, 0.0f,
                                            0.0f, 0.0f, 0.0f, 1.0f };


#endif // GEOMETRY_HPP
