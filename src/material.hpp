#ifndef MATERIAL_HPP
#define MATERIAL_HPP


#include <algorithm>
#include <array>
#include <cstdint>


using color = std::array<float,4>;


inline constexpr color to_color(uint32_t rgb)
{
    const uint8_t r = rgb >> 16;
    const uint8_t g = rgb >> 8;
    const uint8_t b = rgb;

    return color{ r / 255.0f, g / 255.0f, b / 255.0f, 1.0f };
}


inline constexpr color clamp(color c)
{
    return { std::max(std::min(c[0], 1.0f), 0.0f),
             std::max(std::min(c[1], 1.0f), 0.0f),
             std::max(std::min(c[2], 1.0f), 0.0f),
             std::max(std::min(c[3], 1.0f), 0.0f) };
}


inline constexpr color scale(color c, float s)
{
    return clamp({ c[0] * s, c[1] * s, c[2] * s, c[3] });
}


inline constexpr color white{1.0f, 1.0f, 1.0f, 1.0f};
inline constexpr color black{0.0f, 0.0f, 0.0f, 1.0f};

inline constexpr color red  {1.0f, 0.0f, 0.0f, 1.0f};
inline constexpr color green{0.0f, 1.0f, 0.0f, 1.0f};
inline constexpr color blue {0.0f, 0.0f, 1.0f, 1.0f};

inline constexpr color brown                = to_color(0x964b00);
inline constexpr color coral                = to_color(0xff7f50);
inline constexpr color cyan                 = to_color(0x00ffff);
inline constexpr color fluorescent_yellow   = to_color(0xccff00);
inline constexpr color forest_green         = to_color(0x228b22);
inline constexpr color gray                 = to_color(0xa9a9a9);
inline constexpr color olive                = to_color(0x808000);
inline constexpr color orange               = to_color(0xffa500);
inline constexpr color magenta              = to_color(0xcc00cc);
inline constexpr color navy                 = to_color(0x000080);
inline constexpr color pink                 = to_color(0xffc0cb);
inline constexpr color purple               = to_color(0xa020f0);
inline constexpr color sea_green            = to_color(0x8fbc8f);
inline constexpr color yellow               = to_color(0xffff33);

   
struct material
{   
    color ambient = black;
    color diffuse = black;
    color specular = black;
    color emission = black;
    float roughness{0.001f};
};


#endif // MATERIAL_HPP
