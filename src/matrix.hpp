#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <array>
#include <stdexcept>

template<typename T, size_t C>
T dotsum(const std::array<T, C>& a1, const std::array<T, C>& a2)
{
    T r(0);
    for (unsigned int i(0); i < C; ++i)
        r += a1[i] * a2[i];
    
    return r;
}


template <class T, size_t R, size_t C>
struct Matrix : public std::array<std::array<T, C>, R>
{
    Matrix& identity()
    {
        static_assert(R == C, "Square matrix required");

        for (int r(0); r < this->rows(); ++r)
        {
            (*this)[r].fill(0.0);
            (*this)[r][r] = 1.0;
        }

        return *this;
    }

    size_t rows() const { return R; }
    size_t cols() const { return C; }

    std::array<T, C> row(size_t r) const 
    {
        return (*this)[r];
    }

    std::array<T, R> col(size_t c) const
    {
        std::array<T, R> a;

        for (unsigned int i(0); i < this->rows(); ++i)
            a[i] = (*this)[i][c];

        return a;
    }

    Matrix<T, R, 1> colMx(size_t c) const
    {
        Matrix<T, R, 1> r;
        for (int i(0); i < this->rows(); ++i)
            r[i][0] = (*this)[i][c];

        return r;
    }

    Matrix& setRow(size_t r, const std::array<T, C>& row)
    {
        (*this)[r] = row;

        return *this;
    }

    Matrix operator+(const Matrix& m) const 
    {
        Matrix<T, R, C> r;
        for (int i(0); i < this->rows(); ++i)
        {
            for (int j(0); j < this->cols(); ++j)
                r[i][j] = (*this)[i][j] + m[i][j];
        }

        return r;
    }

    template<size_t CL>
    Matrix<T, R, CL> operator*(const Matrix<T, C, CL>& m) const
    {
        Matrix<T, R, CL> r;

        for (unsigned int j(0); j < m.cols(); ++j)
        {
            std::array<T, C> cj = m.col(j);

            for (unsigned int i(0); i < this->rows(); ++i)
            {
                std::array<T, C> ri = this->row(i);
                r[i][j] = dotsum(ri, cj);
            }
        }

        return r;
    }
};

#endif // MATRIX_HPP
