#include "mesh.hpp"


bool facet::is_within(std::array<vector4,2> const& bounds) const
{
    return 
        point1[0] >= bounds[0][0] && point1[0] < bounds[1][0] &&
        point1[1] >= bounds[0][1] && point1[1] < bounds[1][1] &&
        point1[2] >= bounds[0][2] && point1[2] < bounds[1][2] &&
        point2[0] >= bounds[0][0] && point2[0] < bounds[1][0] &&
        point2[1] >= bounds[0][1] && point2[1] < bounds[1][1] &&
        point2[2] >= bounds[0][2] && point2[2] < bounds[1][2] &&
        point3[0] >= bounds[0][0] && point3[0] < bounds[1][0] &&
        point3[1] >= bounds[0][1] && point3[1] < bounds[1][1] &&
        point3[2] >= bounds[0][2] && point3[2] < bounds[1][2];
}


bool adjacent_edges(const facet& t1, uint8_t eix1, const facet& t2, uint8_t eix2)
{
    return adjacent_edges(get_edge(t1, eix1), get_edge(t2, eix2));
}


void connect_edge(std::vector<facet>& triangles, std::unordered_map<edge,edge_id>& edges, edge_id eid)
{
    facet& tr = triangles[eid.first];
    const edge ea = get_edge(tr, eid.second);
    auto fa = edges.find(ea);
    auto fb = edges.find(reverse(ea));

    if (edges.end() != fa || edges.end() != fb)
    {
        edge_id id;

        if (edges.end() != fa && edges.end() != fb)
            throw std::runtime_error("Invalid data: There are multiple edges in one place.");

        if (edges.end() != fa)
        {
            id = fa->second;
            edges.erase(fa);
        }
        else
        {
            id = fb->second;
            edges.erase(fb);
        }

        facet& tre = triangles[id.first];

        if (!adjacent_edges(tr, eid.second, tre, id.second))
            throw std::logic_error("Connecting edges gave bad results.");

        tr.adjacent[eid.second] = id;
        tre.adjacent[id.second] = edge_id{eid.first, eid.second};
    }
    else
    {
        edges[ea] = eid;
    }
}


void detect_adjacent_triangles(std::vector<facet>& triangles)
{
    std::unordered_map<edge,edge_id> edges;

    for (uint32_t ix = 0; ix < triangles.size(); ++ix)
    {
        connect_edge(triangles, edges, {ix, 0});
        connect_edge(triangles, edges, {ix, 1});
        connect_edge(triangles, edges, {ix, 2});
    }
}


void mesh::measure()
{
    if (0 == facets.size())
        return;

    zorder.clear();
    for (size_t i(0); i < facets.size(); ++i)
    {
        float cz = (facets[i].point1[2] + facets[i].point2[2] + facets[i].point3[2]) / 3.0f;
        zorder.push_back({cz, i});
    }
    std::sort(zorder.begin(), zorder.end(), [](std::pair<float,size_t> const& lv, std::pair<float,size_t> const& rv)
    {
        return lv.first < rv.first;
    });

    center = zero_position;

    vector4 l0 = facets[0].point1;
    vector4 lf = l0;

    for (uint32_t i = 0; i < facets.size(); ++i)
    {
        l0 = std::min(l0, facets[i].point1);
        l0 = std::min(l0, facets[i].point2);
        l0 = std::min(l0, facets[i].point3);

        lf = std::max(lf, facets[i].point1);
        lf = std::max(lf, facets[i].point2);
        lf = std::max(lf, facets[i].point3);
    }

    l0[3] = lf[3] = 1.0f;

    limits = { l0, lf };
    center = (l0 + lf) * 0.5f;

    vector4 s = lf - l0;
    size = 10.0f; // TODO ??? Should that be like this?
    size = std::max(size, s[0]);
    size = std::max(size, s[1]);
    size = std::max(size, s[2]);
    size *= 1.3f;

    measured = true;
}


void mesh::load_mesh(const std::string& file_name)
{
    stl_data<facet> data = read_stl_file<facet>(file_name);

    for (uint32_t ix = 0; ix < data.triangles.size(); ++ix)
        data.triangles[ix].org_ix = ix;

    detect_adjacent_triangles(data.triangles);
    measure();

    header = std::move(data.header);
    facets = std::move(data.triangles);
}


void mesh::store_mesh(const std::string& file_name) const
{
    stl_data<facet> data{ header, facets };

    write_stl_file(file_name, data);
}


void mesh::transform(matrix4 const& mx)
{
    for (size_t i(0); i < facets.size(); ++i)
    {
        facet& f = facets[i];
        f.point1 = mx * f.point1;
        f.point2 = mx * f.point2;
        f.point3 = mx * f.point3;
        f.normal = mx * f.normal;
    }
}


void mesh::filter(std::array<vector4,2> const& bounds, std::vector<size_t>& ind) const
{
    for (size_t i(0); i < facets.size(); ++i)
    {
        if (facets[i].is_within(bounds))
            ind.push_back(i);
    }
}


void mesh::filter(std::array<vector4,2> const& bounds, std::array<vector4,2> const& forbidden, std::vector<size_t>& ind) const
{
    for (size_t i(0); i < facets.size(); ++i)
    {
        if (facets[i].is_within(bounds) && !facets[i].is_within(forbidden))
            ind.push_back(i);
    }
}


