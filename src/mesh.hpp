#ifndef MESH_HPP
#define MESH_HPP


#include "stl.hpp"

#include <functional>
#include <limits>
#include <unordered_map>


using edge = std::pair<vector4,vector4>;
using edge_id = std::pair<uint32_t,uint8_t>; // triangle ix, edge ix


constexpr inline edge_id unconnected = { std::numeric_limits<uint32_t>::max(),
                                         std::numeric_limits<uint8_t>::max() };


inline edge reverse(const edge& e)
{
    return { e.second, e.first };
}


inline bool adjacent_edges(const edge& e1, const edge& e2)
{
    auto [v1a,v1b] = e1;
    auto [v2a,v2b] = e2;

    return (v1a == v2a && v1b == v2b) || (v1a == v2b && v1b == v2a);
}


namespace std {


// Specialization of std::hash for edge-type value.
template<>
struct hash<edge>
{
    inline std::size_t operator()(const edge& e) const noexcept
    {
        return f(e.first[0]) ^ f(e.first[1]) ^ f(e.first[2]) ^ f(e.first[3]) ^
               f(e.second[0]) ^ f(e.second[1]) ^ f(e.second[2]) ^ f(e.second[3]);
    }

private:
    std::hash<float> f;
};


} // namespace std




struct facet : public triangle
{
//  Part of triangle.
//  vector4 point1 = zero_vector;
//  vector4 point2 = zero_vector;
//  vector4 point3 = zero_vector;
//  vector4 normal = zero_vector;
//  uint16_t attribute{};

    uint32_t org_ix{}; // Index of the facet in the original stl shape.
    std::array<edge_id, 3> adjacent{ unconnected, unconnected, unconnected };

    bool is_within(std::array<vector4,2> const& bounds) const;
};


inline edge get_edge(const facet& triangle, uint8_t eix)
{
    vector4 va = (0 == eix) ? triangle.point1 : ((1 == eix) ? triangle.point2 : triangle.point3);
    vector4 vb = (0 == eix) ? triangle.point2 : ((1 == eix) ? triangle.point3 : triangle.point1);

    return { va, vb };
}


inline edge get_edge(const std::vector<facet>& triangles, edge_id eid)
{
    return get_edge(triangles[eid.first], eid.second);
}


void detect_adjacent_triangles(std::vector<facet>& triangles);


inline vector4 get_center(facet const& tri)
{
    return (tri.point1 + tri.point2 + tri.point3) / 3.0f;
}


struct mesh
{
    std::array<uint8_t, 80> header{}; // Header of related stl file.

    std::vector<facet> facets;
    std::vector<std::pair<float, std::size_t>> zorder;

    bool measured{};
    float size{};
    std::array<vector4,2> limits = { zero_position, zero_position };
    vector4 center = zero_position;

    void load_mesh(const std::string& file_name);
    void store_mesh(const std::string& file_name) const;

    void measure();
    void transform(matrix4 const& mx);

    void filter(std::array<vector4,2> const& bounds, std::vector<size_t>& ind) const;
    void filter(std::array<vector4,2> const& bounds, std::array<vector4,2> const& forbidden, std::vector<size_t>& ind) const;
//  int findCommonEdge(vector3d const v1, vector3d const v2, vector3d const v3) const;
};


#endif // MESH_HPP
