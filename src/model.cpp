#include "model.hpp"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
#include <stdexcept>


using namespace std;


void model::add_neighbours(size_t part, size_t facetidx)
{
    facet const& f = orgmesh.facets[facetidx];
    if (parts[part].size() < 100000)
    {
        for (int k(0); k < 3; ++k)
        {
            if (f.adjacent[k] != unconnected)
            {
                if (std::find(std::begin(parts[part]), std::end(parts[part]), f.adjacent[k].first) == std::end(parts[part]))
                {
                    parts[part].push_back(f.adjacent[k].first);
                    add_neighbours(part, f.adjacent[k].first);
                }
            }
        }
    }
}


void model::find_centers(std::array<vector4, 2> const& bounds)
{
    auto forbidden = bounds;
    forbidden[0][0] += 15.0f;
    forbidden[1][0] -= 15.0f;
    forbidden[1][1] = forbidden[0][1] + 10.0f;
    std::vector<size_t> slice;
    orgmesh.filter(bounds, forbidden, slice);
    std::cout << slice.size() << " facets in slice\n";

    num_assigned = 0;
    //bool ok = false;
    //while (!ok)
    //{
    while (num_assigned < nteeth && slice.size() > 0)
    {
        auto it = slice.begin();
        size_t index = *it;
        slice.erase(it);

        num_assigned++;
        std::vector<size_t>& v = parts[num_assigned-1];
        v.clear();
        v.push_back(index);

        for (size_t vi(0); vi < v.size(); ++vi)
        {
            size_t ii = v[vi];
            facet& f = orgmesh.facets[ii];

            bool found_adj = false;
            for (size_t k(0); k < 3; ++k)
            {
                if (f.adjacent[k] != unconnected)
                {
                    auto it = std::find(std::begin(slice), std::end(slice), f.adjacent[k].first);
                    if (it != std::end(slice))
                    {
                        size_t fidx = f.adjacent[k].first;
                        //float dxy = distancexy(get_center(orgmesh.facets[fidx]), get_center(orgmesh.facets[v[0]]));
                        float dz = fabs(bounds[0][2] - get_center(orgmesh.facets[fidx])[2]);
                        float vang = angle(orgmesh.facets[fidx].normal, {0, 0, -1, 0});
                        //if (dz > 2.0f && dxy > 6.0f && fabs(vang - 90) < 10.0f)
                        if (dz > 2.0f && fabs(vang - 90) < 14.0f)
                        {
                            slice.erase(it);
                            continue;
                        }
                        float ang = angle(f.normal, orgmesh.facets[fidx].normal);
                        if (fabs(ang) < 63.0f)
                        {
                            v.push_back(fidx);
                            slice.erase(it);
                            found_adj = true;
                        }
                    }
                }
            }

            if (!found_adj)
            {
                /*
                // find the closest triangle
                auto cc = get_center(orgmesh.facets[index]);
                float mind = 1e6;
                auto fit = slice.end();

                auto it2(it); 
                ++it2;
                while (it2 != slice.end())
                {
                    auto ci = get_center(orgmesh.facets[*it2]);
                    float d = distancexy(cc, ci);
                    if (d < mind)
                    {
                        mind = d;
                        fit = it2;
                    }
                    ++it2;
                }
                if (fit != slice.end())
                {
                    //float ang = angle(f.normal, orgmesh.facets[*fit].normal);
                    if (mind <= 3.0f)
                    {
                        v.push_back(*fit);
                        slice.erase(fit);
                    }
                }*/
            }
        }
        if (v.size() < 200)
        {
            v.clear();
            --num_assigned;
        }
    }

    std::cout << "Found " << num_assigned << " subsets\n";

    for (size_t i(0); i < num_assigned; ++i)
    {
        std::cout << parts[i].size() << " facets in part " << (i+1) << std::endl;

        teethcenter[i] = zero_position;
        for (size_t idx : parts[i])
        {
            teethcenter[i] = teethcenter[i] + orgmesh.facets[idx].point1 + orgmesh.facets[idx].point2 + orgmesh.facets[idx].point3;
        }
        if (parts[i].size() > 0)
            teethcenter[i] = teethcenter[i] / (3 * parts[i].size());
    }
}


void model::partition(std::array<vector4, 2> const& bounds)
{
    auto forbidden = bounds;
    forbidden[0][0] += 25.0f;
    forbidden[1][0] -= 25.0f;
    forbidden[1][1] = forbidden[0][1] + 10.0f;
    std::vector<size_t> slice;
    orgmesh.filter(bounds, forbidden, slice);

    for (size_t i(0); i < num_assigned; ++i)
        parts[i].clear();

    for (size_t index : slice)
    {
        facet const& f = orgmesh.facets[index];

        // check if any part has f's neighbour (and if the geometric conditions are fulfilled)
        int partidx = find_matching_part(f);

        if (partidx == -1)
        {
            // none found -> put the facet into the closest part
            auto fc = get_center(f);
            partidx = find_closest(fc, f.normal);
        }

        if (partidx >= 0 && partidx < static_cast<int>(num_assigned))
            parts[partidx].push_back(index);
        else 
        {
            if (partidx == static_cast<int>(nteeth))
                gums.push_back(index);
        }
    }
}


int model::find_matching_part(facet const& f)
{
    int matching = -1;

    (void)f;

    return matching;
}


int model::find_closest(vector4 const& p, vector4 const& n)
{
    float mind = 1e6;
    int closest = -1;

    for (size_t i(0); i < num_assigned; ++i)
    {
        float d = distancexy(p, teethcenter[i]);
        if (d < mind)
        {
            mind = d;
            closest = i;
        }
    }

    // check the angle to z axis if the z-distance from the center is greater than ...
    float dz = fabs(teethcenter[closest][2] - p[2]);
    if (dz > 4.0f)
    {
        float vang = angle(n, {0, 0, -1, 0});
        if (vang < 45.0f || vang > 100.0f)
            return mind < maxgd ? nteeth : -1;
    }
    else if (dz > 9.0f)
        return mind < maxgd ? nteeth : -1;

    return mind <= maxcd ? closest : (mind < maxgd ? nteeth : -1);
}


