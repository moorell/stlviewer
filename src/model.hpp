#ifndef MODEL_HPP
#define MODEL_HPP


#include "geometry.hpp"
#include "material.hpp"
#include "mesh.hpp"

#include <vector>

struct model
{
    model() {};
    model(mesh const& _mesh) : orgmesh(_mesh) {};

    void set_mesh(mesh const& _mesh) {
        orgmesh = _mesh;
    }

    void add_material(material const& m) {
        materials.push_back(m);
    }

    void find_centers(std::array<vector4, 2> const& bounds);
    
    void partition(std::array<vector4, 2> const& bounds);
    
    void add_neighbours(size_t part, size_t facetidx);

    int find_closest(vector4 const& p, vector4 const& n);

    int find_matching_part(facet const& f);

    //

    static size_t constexpr nteeth = 16;
    float maxcd = 6.0f;
    float maxgd = 20.0f;

    mesh orgmesh;

    size_t num_assigned{};
    vector4 teethcenter[nteeth];
    std::vector<size_t> parts[nteeth];
    std::vector<size_t> gums;

    std::vector<material> materials;
};

#endif
