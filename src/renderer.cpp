#include <GL/gl.h>
#include <GL/glut.h>

#include "renderer.hpp"

typedef struct light {
    float ambient[4];
    float diffuse[4];
    float specular[4];
    float position[4];
} light;


void renderer::draw(model const& mdl)
{
    if (curr_model != &mdl)
    {
        curr_model = &mdl;
    }

    for (size_t i(0); i < mdl.nteeth; ++i)
    {
        if (mdl.parts[i].size() > 0)
            draw_mesh(mdl.orgmesh, mdl.parts[i], i);
    }
    if (mdl.gums.size() > 0)
        draw_mesh(mdl.orgmesh, mdl.gums);

    draw_mesh(mdl.orgmesh, subsetind);
}


void renderer::set_material(material const& mat)
{
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat.ambient.data());
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat.diffuse.data());
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat.specular.data());
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat.emission.data());
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat.roughness);
}


void renderer::set_subset(std::vector<size_t> const& ind)
{
    subsetind = ind;
}


void renderer::reset_subset()
{
    subsetind.clear();
}


void renderer::draw_mesh(mesh const& m, std::vector<size_t> const& ind, int materialIndex)
{
    set_light();

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    glPushMatrix();

    // Rotate model and view from a depth of twice its size
    glTranslatef(0.0f, 0.0f, -2.0 * curr_model->orgmesh.size);
    glRotatef(yaw, 0.0f, 1.0f, 0.0f); //yaw about y-axis
    glRotatef(pitch, 1.0f, 0.0f, 0.0f); //pitch about x-axis 
    glTranslatef(0.0f, 0.0f, zoff);

    //Move to centre object in screen
    auto center = curr_model->orgmesh.center;
    glTranslatef(-center[0], -center[1], -center[2]);

    if (materialIndex >= 0)
        set_material(curr_model->materials[materialIndex]);
    else
    {
        GLfloat ka[] = {0.3f, 0.3f, 0.3f, 1.0f};
        GLfloat kd[] = {0.5f, 0.5f, 0.0f, 1.0f};
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ka);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, kd);
    }

    glDisable(GL_TEXTURE_2D);

    glShadeModel(GL_SMOOTH);
    //glShadeModel(GL_FLAT);

    if (m.facets.size() > 0)
    {
        glBegin(GL_TRIANGLES);

        if (ind.size() == 0)
        {
            for (size_t i(0); i < m.facets.size(); ++i)
            {
                glNormal3fv(const_cast<float*>(m.facets[i].normal.data()));
                glVertex3fv(const_cast<float*>(m.facets[i].point1.data()));
                glVertex3fv(const_cast<float*>(m.facets[i].point2.data()));
                glVertex3fv(const_cast<float*>(m.facets[i].point3.data()));
            }
        }
        else
        {
            for (size_t i : ind)
            {
                glNormal3fv(const_cast<float*>(m.facets[i].normal.data()));
                glVertex3fv(const_cast<float*>(m.facets[i].point1.data()));
                glVertex3fv(const_cast<float*>(m.facets[i].point2.data()));
                glVertex3fv(const_cast<float*>(m.facets[i].point3.data()));
            }
        }
        glEnd();
    }

    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);

    glPopMatrix();
}

void renderer::set_light()
{
    // Neutral light from upper, right, front
    static light light0 = {
        { 0.2f, 0.5f, 1.0f, 1.0f, },
        { 1.0f, 1.0f, 1.0f, 1.0f, },
        { 1.0f, 1.0f, 1.0f, 1.0f, },
        { 1.0f, 1.0f, 1.0f, 0.0f, },
    };

    // Yellowish light from lower, left, side
    static light light1 = {
        { 1.0f, 0.5f, 0.2f, 1.0f, },
        { 1.0f, 1.0f, 0.5f, 1.0f, },
        { 1.0f, 1.0f, 0.5f, 1.0f, },
        { -1.0f, -1.0f, 0.0f, 0.0f, }, 
    };

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light0.ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light0.diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light0.specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light0.position);

    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light1.ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light1.diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light1.specular);
    glLightfv(GL_LIGHT1, GL_POSITION, light1.position);
}

