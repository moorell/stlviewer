#ifndef renderer_hpp
#define renderer_hpp

#include <vector>

#include "model.hpp"

class renderer
{
public:
    renderer() : curr_model(nullptr) {}

    void draw(model const& mdl);

    void set_view_params(float y, float p, float z)
    {
        yaw = y;
        pitch = p;
        zoff = z;
    }

    void set_subset(std::vector<size_t> const& ind);
    void reset_subset();

private:
    void set_material(material const& mat);
    void draw_mesh(mesh const& m, std::vector<size_t> const& ind, int materialIndex = -1);
    void set_light();

private:
    model const* curr_model;
    std::vector<size_t> subsetind;
    float yaw;
    float pitch;
    float zoff;
};

#endif // renderer_hpp
