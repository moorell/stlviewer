#include "stl.hpp"


template
stl_data<triangle> read_stl_file(const std::string& file_name);


template
void write_stl_file(const std::string& file_name, const stl_data<triangle>& data);
