#ifndef STL_HPP
#define STL_HPP


#include "geometry.hpp"

#include <fstream>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>


struct triangle
{
    vector4 point1 = zero_position;
    vector4 point2 = zero_position;
    vector4 point3 = zero_position;
    vector4 normal = zero_position;
    uint16_t attribute{};
};


template <typename T = triangle>
struct stl_data
{
    std::array<uint8_t, 80> header;
    std::vector<T> triangles;
};


template <typename T = triangle>
stl_data<T> read_stl_file(const std::string& file_name)
{
    stl_data<T> data;
    std::ifstream ifs(file_name);

    ifs.read(reinterpret_cast<char*>(data.header.data()), data.header.size());

    uint32_t triangle_count;

    ifs.read(reinterpret_cast<char*>(&triangle_count), sizeof(uint32_t));

    data.triangles.reserve(triangle_count);

    for (uint32_t i = 0; i < triangle_count; ++i)
    {
        T tr{};

        ifs.read(reinterpret_cast<char*>(tr.normal.data()), sizeof(vector4::value_type) * 3);
        ifs.read(reinterpret_cast<char*>(tr.point1.data()), sizeof(vector4::value_type) * 3);
        ifs.read(reinterpret_cast<char*>(tr.point2.data()), sizeof(vector4::value_type) * 3);
        ifs.read(reinterpret_cast<char*>(tr.point3.data()), sizeof(vector4::value_type) * 3);
        ifs.read(reinterpret_cast<char*>(&tr.attribute), sizeof(tr.attribute));

        data.triangles.push_back(tr);
    }

    return data;
}


template <typename T = triangle>
void write_stl_file(const std::string& file_name, const stl_data<T>& data)
{
    std::ofstream ofs(file_name);

    ofs.write(reinterpret_cast<const char*>(data.header.data()), data.header.size());

    if (std::numeric_limits<uint32_t>::max() < data.triangles.size())
        throw std::logic_error("Mesh contains too many triangles - cannot fit into STL format limits.");

    const uint32_t triangle_count = data.triangles.size();

    ofs.write(reinterpret_cast<const char*>(&triangle_count), sizeof(uint32_t));

    for (uint32_t i = 0; i < triangle_count; ++i)
    {
        const T& tr = data.triangles[i];

        ofs.write(reinterpret_cast<const char*>(tr.normal.data()), sizeof(vector4::value_type) * 3);
        ofs.write(reinterpret_cast<const char*>(tr.point1.data()), sizeof(vector4::value_type) * 3);
        ofs.write(reinterpret_cast<const char*>(tr.point2.data()), sizeof(vector4::value_type) * 3);
        ofs.write(reinterpret_cast<const char*>(tr.point3.data()), sizeof(vector4::value_type) * 3);
        ofs.write(reinterpret_cast<const char*>(&tr.attribute), sizeof(tr.attribute));
    }
}


#endif // STL_HPP
