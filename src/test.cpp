#include "model.hpp"

#include <iostream>

int main(int argc, char** argv)
{
    Model m;
    m.loadFromFile(argv[1]);

    std::cout << m.getFacetCount() << std::endl;
}
