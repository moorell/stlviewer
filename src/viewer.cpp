#include "mesh.hpp"
#include "model.hpp"
#include "renderer.hpp"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <array>
#include <chrono>
#include <cmath>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <GL/glut.h>


float pitch = 0.0f;
float yaw = 180.0f;
float z = 0.0f;
float pitch0, yaw0;
int MousePressed = 0;
int mouseX0, mouseY0;
int Animated = 0;
int Wireframe = 0;
int Textures = 0;

// position and analysis window
float iroty = -3.0f;
float irotx = -2.6f;
float roty = 0.0f;
float rotx = 0.0f;
float zcut = 0.0f;
float zthick = 3.15f;
float maxz = 14.0f;

// perspective
float fov = 45.0;
float Ar;
float Zn = 1;
float Zf;

bool verbose = false;

stl_data<facet> data;
renderer r;
model m;

void display(void)
{
    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (Wireframe)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    r.draw(m);

    glutSwapBuffers();
}

void advanceAnimation(void)
{
    if (!MousePressed && Animated) 
    {
        yaw += 1.0f;
        r.set_view_params(yaw, pitch, z);
    }
}

void idle(void)
{
    advanceAnimation();
    glutPostRedisplay();
}

void setPerspective()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fov, Ar, Zn, Zf);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glTranslatef(0.0, 0.0, -3.0);
}

void positionModel(float drx, float dry)
{
    rotx += drx;
    roty += dry;

    matrix4 mxy = create_roty_matrix(dry);
    matrix4 mxx = create_rotx_matrix(drx);
    auto mx = mxy * mxx;
    m.orgmesh.transform(mx);
    m.orgmesh.measure();

    std::cout << "Rot: " << rotx << ", " << roty << std::endl;
}

void filter_model()
{
    auto bounds = m.orgmesh.limits;
    bounds[0][2] += zcut;
    bounds[1][2] = bounds[0][2] + zthick;
    std::vector<size_t> slice;
    m.orgmesh.filter(bounds, slice);

    std::cout << "Cut: " << zcut << ", " << zthick << std::endl;

    r.set_subset(slice);
}

void find_centers()
{
    auto bounds = m.orgmesh.limits;
    bounds[0][2] += zcut;
    bounds[1][2] = bounds[0][2] + zthick;

    //std::thread thr([&]() {
        m.find_centers(bounds);
    //});

    std::vector<size_t> slice;
    slice.push_back(0);
    r.set_subset(slice);
}

void partition()
{
    auto bounds = m.orgmesh.limits;
    bounds[1][2] = bounds[0][2] + maxz;

    m.partition(bounds);
}

void keyboard(unsigned char key, int x, int y)
{
    (void)x;
    (void)y;

    switch (key) 
    {
        case 'a':
            Animated = !Animated;
            break;
        case 'w':
            Wireframe = !Wireframe;
            break;
        case 'n':
            Zn -= 0.1;
            setPerspective();
            break;
        case 'N':
            Zn -= 0.1;
            setPerspective();
            break;
        case 'f':
            Zf -= 0.1;
            setPerspective();
            break;
        case 'F':
            Zf -= 0.1;
            setPerspective();
            break;
        case 'x':
            positionModel(-0.1f, 0.0f);
            filter_model();
            break;
        case 'X':
            positionModel(0.1f, 0.0f);
            filter_model();
            break;
        case 'y':
            positionModel(0.0f, -0.1f);
            filter_model();
            break;
        case 'Y':
            positionModel(0.0f, 0.1f);
            filter_model();
            break;
        case 'z':
            z += 1;
            r.set_view_params(yaw, pitch, z);
            break;
        case 'Z':
            z -= 1;
            r.set_view_params(yaw, pitch, z);
            break;
        case 'c':
            zcut -= 0.05f;
            filter_model();
            break;
        case 'C':
            zcut += 0.05f;
            filter_model();
            break;
        case 't':
            zthick -= 0.05f;
            filter_model();
            break;
        case 'T':
            zthick += 0.05f;
            filter_model();
            break;
        case 'r':
            find_centers();
            break;
        case 'R':
            partition();
            break;
        case 'q':
        case 27:
            exit(0);
            break;
    }

    glutPostRedisplay();
}

void menu(int item)
{
    keyboard((unsigned char) item, 0, 0);
}

void createMenu()
{
    glutCreateMenu(menu);
    glutAddMenuEntry("[a] Toggle animation", 'a');
    glutAddMenuEntry("[w] Toggle wireframe/filled", 'w');
    //glutAddMenuEntry("[t] Texture", 't');
    glutAddMenuEntry("", 0); // add blnk line
    glutAddMenuEntry("[q] Quit", 27);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

void mouse(int button, int state, int x, int y)
{
    (void)button;

    switch (state)
    {
        case GLUT_DOWN:
            MousePressed = 1;
            pitch0 = pitch;
            yaw0 = yaw;
            mouseX0 = x;
            mouseY0 = y;
            r.set_view_params(yaw, pitch, z);
            break;
        default:
        case GLUT_UP:
            MousePressed = 0;
            break;
    }
} 

// Call when mouse moved with left button down
void motion(int x, int y)
{
    // mouse button pressed so:
    pitch = pitch0 + (y - mouseY0);
    yaw = yaw0 + (x - mouseX0);
    r.set_view_params(yaw, pitch, z);
} 

void reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    Zf = 3 * m.orgmesh.size;
    Ar = (float)width/height;
    gluPerspective(fov, Ar, Zn, Zf);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //glTranslatef(0.0, 0.0, -3.0);
}


stl_data<facet> readModel(const std::string& fileName)
{
    stl_data<facet> data;

    {
        auto start = std::chrono::steady_clock::now();
        data = read_stl_file<facet>(fileName);
        auto end = std::chrono::steady_clock::now();

        std::chrono::duration<double> diff = end - start;
        std::cout << "Full model read-in time: " << diff.count() << " s\n";
    }

    for (uint32_t ix = 0; ix < data.triangles.size(); ++ix)
        data.triangles[ix].org_ix = ix;

    {
        auto start = std::chrono::steady_clock::now();
        detect_adjacent_triangles(data.triangles);
        auto end = std::chrono::steady_clock::now();

        std::chrono::duration<double> diff = end - start;
        std::cout << "Full model adjacent edge detection time: " << diff.count() << " s\n";
    }
    
    return data;
}

void dropModel(stl_data<facet>& data, bool fullDrop)
{
    std::cout << "Header: [";
    for (uint8_t b : data.header)
    {
        if (0 != b)
            std::cout << b;
        else
            std::cout << "X";
    }
    std::cout << "]" << std::endl;

    std::cout << "Triangle count: " << data.triangles.size() << std::endl;

    uint32_t zero_attribute_count = 0;
    uint32_t identical_normal_count = 0;
    uint32_t connected_edges = 0;
    uint32_t matched_connected_edges = 0;

    for (uint32_t i = 0; i < data.triangles.size(); ++i)
    {
        facet& tr = data.triangles[i];

        const vector4 vec_s = tr.point2 - tr.point1;
        const vector4 vec_e = tr.point3 - tr.point1;

        vector4 norm = normal(vec_s, vec_e);

        if (norm == tr.normal)
            ++identical_normal_count;

        if (0 == tr.attribute)
            ++zero_attribute_count;

        if (unconnected != tr.adjacent[0])
        {
            ++connected_edges;
            if (adjacent_edges({tr.point1, tr.point2}, get_edge(data.triangles, tr.adjacent[0])))
                ++matched_connected_edges;
        }
        if (unconnected != tr.adjacent[1])
        {
            ++connected_edges;
            if (adjacent_edges({tr.point2, tr.point3}, get_edge(data.triangles, tr.adjacent[1])))
                ++matched_connected_edges;
        }
        if (unconnected != tr.adjacent[2])
        {
            ++connected_edges;
            if (adjacent_edges({tr.point3, tr.point1}, get_edge(data.triangles, tr.adjacent[2])))
                ++matched_connected_edges;
        }

        if (fullDrop)
        {
            std::cout << "Triangle " << i << " (org_ix: " << tr.org_ix << ")\n";
            std::cout << "\tvertex 1: " << tr.point1[0] << " : " << tr.point1[1] << " : " << tr.point1[2] << '\n';
            std::cout << "\tvertex 2: " << tr.point2[0] << " : " << tr.point2[1] << " : " << tr.point2[2] << '\n';
            std::cout << "\tvertex 3: " << tr.point3[0] << " : " << tr.point3[1] << " : " << tr.point3[2] << '\n';

            std::cout << "\tvector s: " << vec_s[0] << " : " << vec_s[1] << " : " << vec_s[2] << '\n';
            std::cout << "\tvector e: " << vec_e[0] << " : " << vec_e[1] << " : " << vec_e[2] << '\n';

            std::cout << "\tnormals:  " << tr.normal[0] << " : " << tr.normal[1] << " : " << tr.normal[2] << '\n';
            std::cout << "\tnormalc:  " << norm[0] << " : " << norm[1] << " : " << norm[2] << '\n';
            std::cout << "\tnormals len: " << length(tr.normal) << ", normalc len: " << length(norm) << '\n';
            if (tr.normal == norm)
                std::cout << "\tCalculated normal is identical to source normal.\n";
            if (0 != tr.attribute)
                std::cout << "\tattribute value: " << tr.attribute << '\n';

            std::cout << "\tedges: " <<
                                   tr.adjacent[0].first << "-" << static_cast<uint32_t>(tr.adjacent[0].second) << ", " <<
                                   tr.adjacent[1].first << "-" << static_cast<uint32_t>(tr.adjacent[1].second) << ", " <<
                                   tr.adjacent[2].first << "-" << static_cast<uint32_t>(tr.adjacent[2].second) << ".\n";

            if (unconnected != tr.adjacent[0])
            {
                auto [v1,v2] = get_edge(data.triangles, tr.adjacent[0]);
                bool ae = adjacent_edges({tr.point1, tr.point2}, {v1, v2});
                std::cout << "\t\tforeign edge 1: " << (ae ? "MATCH" : "FAILED MATCH");
                std::cout << ", start: ";
                std::cout << v1[0] << " : " << v1[1] << " : " << v1[2];
                std::cout << ", end: ";
                std::cout << v2[0] << " : " << v2[1] << " : " << v2[2] << '\n';
            }

            if (unconnected != tr.adjacent[1])
            {
                auto [v1,v2] = get_edge(data.triangles, tr.adjacent[1]);
                bool ae = adjacent_edges({tr.point2, tr.point3}, {v1, v2});
                std::cout << "\t\tforeign edge 2: " << (ae ? "MATCH" : "FAILED MATCH");
                std::cout << ", start: ";
                std::cout << v1[0] << " : " << v1[1] << " : " << v1[2];
                std::cout << ", end: ";
                std::cout << v2[0] << " : " << v2[1] << " : " << v2[2] << '\n';
            }

            if (unconnected != tr.adjacent[2])
            {
                auto [v1,v2] = get_edge(data.triangles, tr.adjacent[2]);
                bool ae = adjacent_edges({tr.point3, tr.point1}, {v1, v2});
                std::cout << "\t\tforeign edge 3: " << (ae ? "MATCH" : "FAILED MATCH");
                std::cout << ", start: ";
                std::cout << v1[0] << " : " << v1[1] << " : " << v1[2];
                std::cout << ", end: ";
                std::cout << v2[0] << " : " << v2[1] << " : " << v2[2] << '\n';
            }

            std::cout << std::endl;
        }
    }

    std::cout << "Count of connected edges: " << connected_edges << " out of total " <<
                 (3 * data.triangles.size()) << std::endl;
    std::cout << "Count of correctly matched edges: " << matched_connected_edges << "." << std::endl;
    if (connected_edges == matched_connected_edges)
        std::cout << "All edges match correctly." << std::endl;
    else
        std::cout << "ERROR: Some edges DO NOT match correctly!" << std::endl;

    if (identical_normal_count == data.triangles.size())
        std::cout << "All calculated normals are identical to source normals." << std::endl;
    else
        std::cout << "INFO: Some calculated normals are different from source normals." << std::endl;

    if (zero_attribute_count == data.triangles.size())
        std::cout << "All attributes are equal zero." << std::endl;
    else
        std::cout << "INFO: Some attributes are non-zero." << std::endl;
}

int main(int argc, char** argv)
{
    //bool optimize = false;

    glutInit(&argc, argv); 

    std::string fileName;
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-v") == 0)
            verbose = true;
        else if (argv[i][0] != '-')
            fileName = argv[i];
    }


    data = readModel(fileName);
    dropModel(data, false);

    mesh inmesh;
    inmesh.header = data.header;
    inmesh.facets = data.triangles;
    m.set_mesh(inmesh);

    positionModel(irotx, iroty);
    filter_model();

    m.add_material({scale(red, 0.1f), red});
    m.add_material({scale(green, 0.1f), green});
    m.add_material({scale(brown, 0.1f), brown});
    m.add_material({scale(coral, 0.1f), coral});
    m.add_material({scale(cyan, 0.1f), cyan});
    m.add_material({scale(fluorescent_yellow, 0.1f), fluorescent_yellow});
    m.add_material({scale(forest_green, 0.1f), forest_green});
    m.add_material({scale(gray, 0.1f), gray});
    m.add_material({scale(olive, 0.1f), olive});
    m.add_material({scale(orange, 0.1f), orange});
    m.add_material({scale(magenta, 0.1f), magenta});
    m.add_material({scale(navy, 0.1f), navy});
    m.add_material({scale(pink, 0.1f), pink});
    m.add_material({scale(purple, 0.1f), purple});
    m.add_material({scale(sea_green, 0.1f), sea_green});
    m.add_material({scale(yellow, 0.1f), yellow});

    /*
    bounds[5] = bounds[2] + 3.0f;
    model.partition(bounds);

    bounds[5] = bounds[2] + 16.0f;

    std::thread repart_thr([&]() {
        model.repartition(bounds);
    });*/

    glutInitWindowPosition(1024, 512);
    glutInitWindowSize(512, 512);

    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    glutCreateWindow("Model viewer");

    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutIdleFunc(idle);

    createMenu();

    r.set_view_params(yaw, pitch, z);
    glutMainLoop();

    //repart_thr.join();

    return 0;
}

